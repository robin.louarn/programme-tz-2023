import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
  testDir: './e2e',
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: process.env.CI
    ? [
        ['list', { printSteps: true }],
        [
          'html',
          {
            open: 'never',
            outputFile: 'index.html',
            outputFolder: 'playwright-report'
          }
        ],
        ['junit', { outputFile: 'playwright-report/results.xml' }]
      ]
    : [['list', { printSteps: true }]],
  use: {
    baseURL: 'http://127.0.0.1:3000',
    trace: 'on-first-retry'
  },
  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] }
    }
  ]
});
