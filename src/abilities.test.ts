import { ForbiddenError, subject } from '@casl/ability';
import { faker } from '@faker-js/faker';
import { Speaker, User } from '@prisma/client';
import { createTranslator } from 'next-intl';
import { describe, expect, test } from 'vitest';
import { EventSubject, ScheduleSubject, defineAbilityFor } from './abilities';
import { defaultLocale } from './i18n/request';
import messages from './messages/fr.json';

const userId = faker.string.uuid();
const userId2 = faker.string.uuid();
const scheduleId = faker.string.uuid();
const eventId = faker.string.uuid();
const formatId = faker.string.uuid();
const trackId = faker.string.uuid();

const user1: User = {
  id: userId,
  name: faker.person.fullName(),
  email: faker.internet.email(),
  image: null,
  emailVerified: null
};

const user2: User = {
  id: userId2,
  name: faker.person.fullName(),
  email: faker.internet.email(),
  image: null,
  emailVerified: null
};

const speaker2: Speaker = {
  userId: userId2,
  bio: null,
  jobTitle: faker.person.jobTitle(),
  company: faker.company.name(),
  companyLogoUrl: null
};

const event: EventSubject = {
  id: eventId,
  dateEnd: new Date(),
  dateStart: new Date(),
  name: faker.lorem.word(),
  updatedAt: new Date(),
  organizers: [
    {
      userId,
      eventId
    }
  ]
};

const schedule: ScheduleSubject = {
  id: scheduleId,
  abstract: faker.lorem.paragraphs(),
  title: faker.lorem.sentence(),
  categoryId: null,
  dateEnd: new Date(),
  dateStart: new Date(),
  durationMinutes: 20,
  eventId,
  formatId,
  language: null,
  level: null,
  tags: [],
  trackId: trackId,
  event,
  speakers: [speaker2]
};

describe('test perms', async () => {
  const t = createTranslator({
    locale: defaultLocale,
    messages,
    namespace: 'Permissions'
  });

  const abilitiesForUser1 = await defineAbilityFor(t, userId);
  const abilitiesForUser2 = await defineAbilityFor(t, userId2);
  const abilitiesForNobody = await defineAbilityFor(t, undefined);

  test('can update user', () => {
    expect(abilitiesForUser1.can('update', subject('User', user1))).toBe(true);
  });

  test('can not update user', () => {
    const forbiddenError = ForbiddenError.from(abilitiesForUser1).unlessCan(
      'update',
      subject('User', user2)
    );
    expect(forbiddenError?.message).toBe(t('User.update'));
    expect(abilitiesForUser1.can('update', subject('User', user2))).toBe(false);
  });

  test('can create event', () => {
    expect(abilitiesForUser1.can('create', 'Event')).toBe(true);
  });

  test('can not create event', () => {
    expect(abilitiesForNobody.can('create', 'Event')).toBe(false);
  });

  test('can update event', () => {
    expect(abilitiesForUser1.can('update', subject('Event', event))).toBe(true);
  });

  test('can not update event', () => {
    const forbiddenError = ForbiddenError.from(abilitiesForUser2).unlessCan(
      'update',
      subject('Event', event)
    );
    expect(forbiddenError?.message).toBe(t('Event.update'));
    expect(abilitiesForUser2.can('update', subject('Event', event))).toBe(
      false
    );
  });

  test('can create schedule', () => {
    expect(abilitiesForUser1.can('create', subject('Schedule', schedule))).toBe(
      true
    );
  });

  test('can not create schedule', () => {
    expect(
      abilitiesForNobody.can('create', subject('Schedule', schedule))
    ).toBe(false);
  });

  test('can update schedule as organizers', () => {
    expect(abilitiesForUser1.can('update', subject('Schedule', schedule))).toBe(
      true
    );
  });

  test('can update schedule as speaker', () => {
    expect(abilitiesForUser2.can('update', subject('Schedule', schedule))).toBe(
      true
    );
  });

  test('can not update schedule as nobody', () => {
    expect(
      abilitiesForNobody.can('update', subject('Schedule', schedule))
    ).toBe(false);
  });
});
