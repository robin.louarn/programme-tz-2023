import { ZodError } from 'zod';

export type ActionError<T> =
  | { validation: ZodError<T> }
  | { permission: string };
