import { z } from 'zod';

const formatSchema = z.object({
  id: z.string().min(1),
  name: z.string(),
  durationMinutes: z.number()
});

const trackSchema = z.object({
  id: z.string().min(1),
  name: z.string()
});

const categorySchema = z.object({
  id: z.string().min(1),
  name: z.string(),
  color: z.string()
});

const socialSchema = z.object({
  name: z.enum(['GitHub', 'Twitter']),
  link: z.string(),
  icon: z.string()
});

const speakerSchema = z.object({
  id: z.string().min(1),
  email: z.string().email().min(1),
  name: z.string(),
  photoUrl: z.string().nullable(),
  bio: z.string().nullable(),
  company: z.string().nullable(),
  jobTitle: z.string().nullable(),
  companyLogoUrl: z.string().nullable(),
  socials: z.array(socialSchema)
});

export const sessionSchema = z.object({
  id: z.string(),
  title: z.string(),
  abstract: z.string().nullable(),
  level: z.string().nullable(),
  durationMinutes: z.number(),
  language: z.string().nullable(),
  dateStart: z.string().datetime({
    offset: true
  }),
  dateEnd: z.string().datetime({
    offset: true
  }),
  tags: z.array(z.string()),
  trackId: z.string(),
  formatId: z.string(),
  categoryId: z.string().nullable(),
  speakerIds: z.array(z.string()),
  showInFeedback: z.boolean()
});

const eventSchema = z.object({
  id: z.string(),
  name: z.string(),
  dateStart: z.string().datetime({
    offset: true
  }),
  dateEnd: z.string().datetime({
    offset: true
  }),
  formats: z.array(formatSchema),
  tracks: z.array(trackSchema),
  categories: z.array(categorySchema),
  updatedAt: z.object({
    nanoseconds: z.number()
  })
});

export const openPlannerSchema = z.object({
  event: eventSchema,
  speakers: z.array(speakerSchema),
  sessions: z.preprocess((sessions) => {
    const result = z.array(z.any()).parse(sessions);
    return result.filter((s) => sessionSchema.safeParse(s).success);
  }, z.array(sessionSchema))
});
