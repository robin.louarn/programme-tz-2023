import { z } from 'zod';

const environmentsServer = z.object({
  OPEN_FEEDBACK_ID: z.string(),
  OPENPLANNER_URL: z.string(),
  OPENPLANNER_EVENT_ID: z.string(),
  GOOGLE_CLIENT_SECRET: z.string(),
  RESEND_API_KEY: z.string(),
  RESEND_FROM: z.string()
});

export const environmentsServerParsed = environmentsServer.parse(process.env);
