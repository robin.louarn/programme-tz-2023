import { z } from 'zod';

const environmentsClient = z.object({
  NEXT_PUBLIC_GOOGLE_CLIENT_ID: z.string()
});

export const environmentsClientParsed = environmentsClient.parse({
  NEXT_PUBLIC_GOOGLE_CLIENT_ID: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID
});
