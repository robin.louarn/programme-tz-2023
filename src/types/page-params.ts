export type PropsWithEventId = {
  params: Promise<{ eventId: string }>;
};
