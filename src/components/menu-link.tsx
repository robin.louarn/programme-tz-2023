'use client';
import { Link, useRouter } from '@/i18n/routing';
import { cn } from '@/libs/utils';
import { FC } from 'react';

type MenuLinkProps = Parameters<typeof Link>[0] & {
  onOpenChange: (open: boolean) => void;
  children: React.ReactNode;
  className?: string;
};
export const MenuLink: FC<MenuLinkProps> = ({
  href,
  onOpenChange,
  className,
  children,
  ...props
}) => {
  const router = useRouter();
  return (
    <Link
      href={href}
      onClick={() => {
        router.push(href.toString());
        onOpenChange(false);
      }}
      className={cn(className)}
      {...props}
    >
      {children}
    </Link>
  );
};
