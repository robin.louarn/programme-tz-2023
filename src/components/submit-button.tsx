import { Loader2 } from 'lucide-react';
import { FC, PropsWithChildren } from 'react';
import { useFormContext } from 'react-hook-form';
import { Button, ButtonProps } from './ui/button';

type SubmintButtonProps = PropsWithChildren<ButtonProps>;

export const SubmitButton: FC<SubmintButtonProps> = ({
  children,
  ...props
}) => {
  const {
    formState: { isSubmitting }
  } = useFormContext();
  return (
    <Button type="submit" disabled={isSubmitting} {...props}>
      {isSubmitting && <Loader2 className="mr-2 h-4 w-4 animate-spin" />}
      {children}
    </Button>
  );
};
