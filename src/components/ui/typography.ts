import { cva } from 'class-variance-authority';

export const typographyVariants = cva('', {
  variants: {
    typography: {
      h1: 'scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl',
      h2: 'scroll-m-20 pb-2 text-3xl font-semibold tracking-tight first:mt-0',
      h3: 'scroll-m-20 text-2xl font-semibold tracking-tight',
      h4: 'scroll-m-20 text-xl font-semibold tracking-tight',
      p: 'leading-7 [&:not(:first-child)]:mt-6',
      blockquote: 'mt-6 border-l-2 pl-6 italic',
      muted: 'text-sm text-muted-foreground'
    }
  },
  defaultVariants: {
    typography: 'h1'
  }
});
