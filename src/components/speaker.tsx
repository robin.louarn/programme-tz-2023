import { cn, initials } from '@/libs/utils';
import { PropsWithEventId } from '@/types/page-params';
import { Speaker as PrismaSpeaker, User } from '@prisma/client';
import { FC } from 'react';
import { Link } from '../i18n/routing';
import { Avatar, AvatarFallback, AvatarImage } from './ui/avatar';
import { buttonVariants } from './ui/button';

export type SpeakerProps = PrismaSpeaker & {
  user: User;
} & Awaited<PropsWithEventId['params']>;

export const Speaker: FC<SpeakerProps> = ({
  eventId,
  userId,
  jobTitle,
  user: { name, image }
}) => (
  <div className="flex items-center gap-4">
    <Avatar className="w-6 h-6">
      {image && <AvatarImage src={image} />}
      <AvatarFallback>{initials(name)}</AvatarFallback>
    </Avatar>
    <Link
      href={`/${eventId}/speakers/${userId}`}
      className={cn(buttonVariants({ variant: 'link', size: 'sm' }), 'p-0')}
    >
      <p className="text-sm font-medium leading-none">{name}</p>
      <p className="text-sm text-muted-foreground">{jobTitle}</p>
    </Link>
  </div>
);
