'use client';

import { Button, ButtonProps } from '@/components/ui/button';
import { FC } from 'react';

type CopyButtonProps = ButtonProps & { value: string };

export const CopyButton: FC<CopyButtonProps> = ({ value, ...props }) => {
  return (
    <Button {...props} onClick={() => navigator.clipboard.writeText(value)} />
  );
};
