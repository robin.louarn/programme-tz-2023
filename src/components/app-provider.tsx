'use client';

import { GoogleOneTapAuth } from '@/app/[locale]/components/google-one-tap';
import { Messages, PropsWithLocale, formats, timeZone } from '@/i18n/request';
import { Analytics } from '@vercel/analytics/react';
import { SpeedInsights } from '@vercel/speed-insights/next';
import { Session } from 'next-auth';
import { SessionProvider } from 'next-auth/react';
import { NextIntlClientProvider } from 'next-intl';
import { ThemeProvider as NextThemesProvider } from 'next-themes';
import { FC, PropsWithChildren } from 'react';
import { Toaster } from './ui/toaster';

export type AppProviderProps = PropsWithChildren<
  {
    session: Session | null;
    messages: Messages;
  } & Awaited<PropsWithLocale['params']>
>;

export const AppProvider: FC<AppProviderProps> = ({
  children,
  session,
  messages,
  locale
}) => (
  <NextThemesProvider attribute="class" defaultTheme="system" enableSystem>
    <SessionProvider session={session}>
      <NextIntlClientProvider
        locale={locale}
        messages={messages}
        formats={formats}
        timeZone={timeZone}
      >
        {children}
        <Toaster />
        <Analytics />
        <SpeedInsights />
        <GoogleOneTapAuth />
      </NextIntlClientProvider>
    </SessionProvider>
  </NextThemesProvider>
);
