import { Speaker } from '@/components/speaker';
import { buttonVariants } from '@/components/ui/button';
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle
} from '@/components/ui/card';
import { Link } from '@/i18n/routing';
import { cn } from '@/libs/utils';
import {
  Category as PrismaCategory,
  Speaker as PrismaSpeaker,
  Track as PrismaTrack,
  Schedule,
  User
} from '@prisma/client';
import { VariantProps, cva } from 'class-variance-authority';
import { MapPin } from 'lucide-react';
import { getTranslations } from 'next-intl/server';
import { FC, HTMLAttributes } from 'react';
import { Badge, BadgeProps } from './ui/badge';

export const scheduleCardVariants = cva('', {
  variants: {
    variant: {
      default: '',
      passed: 'bg-accent text-accent-foreground'
    }
  },
  defaultVariants: {
    variant: 'default'
  }
});

export type ScheduleCardProps = VariantProps<typeof scheduleCardVariants> &
  HTMLAttributes<HTMLDivElement> &
  Schedule & {
    category: PrismaCategory | null;
    track: PrismaTrack;
    speakers?: Array<PrismaSpeaker & { user: User }>;
  };

export const ScheduleCard: FC<ScheduleCardProps> = ({
  id,
  title,
  category,
  speakers,
  track,
  className,
  variant,
  eventId
}) => {
  return (
    <Card className={scheduleCardVariants({ variant, className })}>
      <CardHeader className="flex flex-row justify-between">
        <CardTitle>{category && <Category {...category} />}</CardTitle>
        <Track {...track} />
      </CardHeader>
      <CardContent>
        <Link
          href={`/${eventId}/schedules/${id}`}
          className={cn(buttonVariants({ variant: 'link', size: 'sm' }), 'p-0')}
        >
          {title}
        </Link>
      </CardContent>
      <CardFooter className="justify-around">
        {speakers?.map((speaker) => (
          <Speaker eventId={eventId} key={speaker.userId} {...speaker} />
        ))}
      </CardFooter>
    </Card>
  );
};

const Track: FC<PrismaTrack> = async ({ name }) => {
  const t = await getTranslations('Schedule');
  return (
    <p className="text-sm">
      {t.rich('track', {
        name,
        icon: () => <MapPin className="inline" />
      })}
    </p>
  );
};

const Category: FC<PrismaCategory & BadgeProps> = ({ name }) => (
  <Badge variant="secondary">{name}</Badge>
);
