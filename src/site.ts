import { Messages } from '@/i18n/request';

type MainNavigation = Array<{
  key: keyof Messages['Index']['navigation'];
  href: string;
  match: (pathname: string, href: string) => boolean;
}>;

const basicMatch = (pathname: string, href: string) =>
  pathname.startsWith(href);

export const getMainNav = (eventId: string): MainNavigation => [
  {
    key: 'home',
    href: `/${eventId}`,
    match: (pathname: string, href: string) => pathname === href
  },
  {
    key: 'schedules',
    href: `/${eventId}/schedules`,
    match: (pathname: string, href: string) => pathname.startsWith(href)
  },
  {
    key: 'speakers',
    href: `/${eventId}/speakers`,
    match: basicMatch
  }
];

export const siteConfig = {
  name: 'Zenika',
  links: {
    gitlab: {
      owner: 'https://gitlab.com/robin.louarn',
      project: 'https://gitlab.com/robin.louarn/programme-tz-2023'
    }
  }
} as const;
