import { auth } from '@/auth';
import { AppProvider } from '@/components/app-provider';
import { Messages, PropsWithLocale, getMessages } from '@/i18n/request';
import { fontSans } from '@/libs/fonts';
import prisma from '@/libs/prisma';
import { cn } from '@/libs/utils';
import { Metadata } from 'next';
import {
  getFormatter,
  getNow,
  getTimeZone,
  getTranslations
} from 'next-intl/server';
import { notFound } from 'next/navigation';
import { FC, PropsWithChildren } from 'react';

export const revalidate = 3600;

type LocaleLayoutProps = PropsWithChildren<PropsWithLocale>;

export const generateMetadata = async ({
  params
}: PropsWithLocale): Promise<Metadata> => {
  const { locale } = await params;

  const t = await getTranslations({ locale, namespace: 'Index.metadata' });
  const formatter = await getFormatter({ locale });
  const now = await getNow({ locale });
  const timeZone = await getTimeZone({ locale });

  const events = await prisma.event.findMany();

  const keywords = events.flatMap(({ name, dateStart }) => [
    name,
    formatter.dateTime(dateStart, {
      year: 'numeric',
      month: 'short',
      day: 'numeric'
    })
  ]);

  return {
    metadataBase: new URL('http://localhost:3000'),
    title: t('title'),
    description: t('description'),
    keywords: t('keywords', { keywords: [keywords].join(',') }),
    other: {
      currentYear: formatter.dateTime(now, { year: 'numeric' }),
      timeZone: timeZone || 'N/A'
    }
  };
};

const LocaleLayout: FC<LocaleLayoutProps> = async ({ children, params }) => {
  const { locale } = await params;

  const session = await auth();

  let messages: Messages;
  try {
    messages = await getMessages(locale);
  } catch {
    notFound();
  }

  return (
    <html lang={locale} suppressHydrationWarning>
      <head>
        <title>{messages.Index.short_title}</title>
        <link rel="icon" href="/favicon.ico" />
      </head>
      <body
        className={cn(
          'min-h-screen bg-background font-sans antialiased',
          fontSans.variable
        )}
      >
        <AppProvider session={session} messages={messages} locale={locale}>
          <div className="relative flex min-h-screen flex-col">{children}</div>
        </AppProvider>
      </body>
    </html>
  );
};

export default LocaleLayout;
