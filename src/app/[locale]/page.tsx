import { PropsWithLocale } from '@/i18n/request';
import { redirect } from '@/i18n/routing';
import prisma from '@/libs/prisma';
import { FC } from 'react';

type HomePageProps = PropsWithLocale;

const HomePage: FC<HomePageProps> = async ({ params }) => {
  const { locale } = await params;
  const { id } = await prisma.event.findFirstOrThrow({});

  redirect({ href: `/${id}`, locale });

  return <></>;
};

export default HomePage;
