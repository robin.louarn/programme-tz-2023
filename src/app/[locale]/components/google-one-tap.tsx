'use client';

import { environmentsClientParsed } from '@/types/environments-client';
import { signIn, useSession } from 'next-auth/react';
import Script from 'next/script';
import { FC, useEffect, useState } from 'react';

export const GoogleOneTapAuth: FC = () => {
  const { data: session } = useSession();
  const [ready, setReady] = useState(false);

  useEffect(() => {
    const { google } = window;
    if (session || !ready || !google) return;

    google.accounts.id.initialize({
      client_id: environmentsClientParsed.NEXT_PUBLIC_GOOGLE_CLIENT_ID,
      use_fedcm_for_prompt: true,
      callback: async ({ credential }) =>
        signIn('google-one-tap', {
          credential
        })
    });

    google.accounts.id.prompt();

    return () => {
      google.accounts.id.cancel();
    };
  }, [session, ready]);

  if (session) return null;

  return (
    <Script
      id="google-auth-one-tap-script"
      async
      defer
      onReady={() => setReady(true)}
      src="https://accounts.google.com/gsi/client"
    />
  );
};
