import { PropsWithEventId } from '@/types/page-params';
import { FC, PropsWithChildren } from 'react';
import { SiteFooter } from './components/site-footer';
import { SiteHeader } from './components/site-header';

type LocaleLayoutProps = PropsWithChildren<PropsWithEventId>;

const LocaleLayout: FC<LocaleLayoutProps> = async ({ children, params }) => (
  <div className="relative flex min-h-screen flex-col">
    <SiteHeader params={params} />
    <main className="container flex-1">{children}</main>
    <SiteFooter />
  </div>
);

export default LocaleLayout;
