'use client';

import { Button, buttonVariants } from '@/components/ui/button';
import { typographyVariants } from '@/components/ui/typography';
import { Link } from '@/i18n/routing';
import { useTranslations } from 'next-intl';
import { FC } from 'react';

type ErrorProps = {
  error: Error;
  reset: () => void;
};

const Error: FC<ErrorProps> = ({ error, reset }) => {
  const t = useTranslations('Error');

  return (
    <div className="container flex flex-col items-center justify-center">
      <div className="text-center">
        <h1
          className={typographyVariants({
            className: 'mt-10'
          })}
        >
          {t('title')}
        </h1>
        <p
          className={typographyVariants({
            typography: 'h2',
            className: 'mt-5'
          })}
        >
          {error.message}
        </p>
        <Link href="/" className={buttonVariants({ variant: 'link' })}>
          {t('link')}
        </Link>
        <Button onClick={reset}>{t('reset')}</Button>
      </div>
    </div>
  );
};

export default Error;
