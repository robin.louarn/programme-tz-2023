import { buttonVariants } from '@/components/ui/button';
import { Link } from '@/i18n/routing';
import prisma from '@/libs/prisma';
import { siteConfig } from '@/site';
import { PropsWithEventId } from '@/types/page-params';
import { getTranslations } from 'next-intl/server';
import Image from 'next/image';
import { notFound } from 'next/navigation';
import { FC } from 'react';

const EventPage: FC<PropsWithEventId> = async ({ params }) => {
  const t = await getTranslations('Index');

  const { eventId } = await params;

  const event = await prisma.event.findUnique({
    where: {
      id: eventId
    }
  });

  if (!event) notFound();

  const { name } = event;

  return (
    <section className="flex items-center gap-6 flex-col md:flex-row">
      <div className="flex flex-col items-start gap-2">
        <h1 className="text-3xl font-extrabold leading-tight tracking-tighter md:text-4xl">
          {t('title', { name })}
        </h1>
        <p className="max-w-[700px] text-lg text-muted-foreground">
          {t('sub_title')}
        </p>
        <Link
          target="_blank"
          rel="noreferrer"
          href={siteConfig.links.gitlab.project}
          className={buttonVariants()}
        >
          {t('gitlab_button')}
        </Link>
      </div>
      <Image
        alt={t('dumbledore_image')}
        width={500}
        height={500}
        src="/Dumbledore.png"
        priority
      />
    </section>
  );
};

export default EventPage;
