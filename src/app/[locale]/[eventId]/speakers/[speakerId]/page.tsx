import { ScheduleCard } from '@/components/schedule-card';
import { Avatar, AvatarFallback, AvatarImage } from '@/components/ui/avatar';
import { Badge } from '@/components/ui/badge';
import { Separator } from '@/components/ui/separator';
import prisma from '@/libs/prisma';
import { initials } from '@/libs/utils';
import { FC } from 'react';
import Markdown from 'react-markdown';

type SpeakerPageProps = {
  params: Promise<{
    speakerId: string;
  }>;
};

const SpeakerPage: FC<SpeakerPageProps> = async ({ params }) => {
  const { speakerId } = await params;
  const {
    bio,
    schedules,
    socials,
    user: { name, image }
  } = await prisma.speaker.findUniqueOrThrow({
    where: {
      userId: speakerId
    },
    include: {
      user: true,
      schedules: {
        include: {
          category: true,
          format: true,
          track: true
        }
      },
      socials: true
    }
  });

  return (
    <section className="flex flex-col items-center gap-5 m-5">
      <Avatar className="w-20 h-20">
        {image && <AvatarImage src={image} />}
        <AvatarFallback>{initials(name)}</AvatarFallback>
      </Avatar>
      <p>{name}</p>
      <div className="flex gap-2">
        {socials.map(({ name, link }) => (
          <Badge key={name}>
            {name} : {link}
          </Badge>
        ))}
      </div>
      <Markdown>{bio}</Markdown>
      <Separator />
      {schedules.map((schedule) => (
        <ScheduleCard key={schedule.id} {...schedule} />
      ))}
    </section>
  );
};

export default SpeakerPage;
