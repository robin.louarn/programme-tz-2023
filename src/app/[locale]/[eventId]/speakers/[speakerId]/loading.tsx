import { Card, CardContent, CardHeader } from '@/components/ui/card';
import { Separator } from '@/components/ui/separator';
import { Skeleton } from '@/components/ui/skeleton';
import { FC } from 'react';

const SpeakerProgramme: FC = () => (
  <section className="flex flex-col items-center gap-5 m-5">
    <Skeleton className="h-12 w-12 rounded-full" />
    <div className="flex gap-2">
      <Skeleton className="h-5 w-[200px]" />
      <Skeleton className="h-5 w-[200px]" />
    </div>
    {['w-[60%]', 'w-[55%]', 'w-[58%]'].map((w, key) => (
      <Skeleton key={key} className={`h-5 ${w}`} />
    ))}
    <Separator />
    <Card className="w-[400px]">
      <CardHeader>
        <div className="flex flex-row justify-between">
          <Skeleton className="h-5 w-[20%]" />
          <Skeleton className="h-5 w-[20%]" />
        </div>
      </CardHeader>
      <CardContent>
        <Skeleton className="h-5 w-full" />
      </CardContent>
    </Card>
  </section>
);

export default SpeakerProgramme;
