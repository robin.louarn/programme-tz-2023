import { Skeleton } from '@/components/ui/skeleton';
import { FC } from 'react';

const SpeakersProgramme: FC = () => (
  <section className="flex flex-col flex-nowrap sm:flex-row sm:flex-wrap justify-center gap-5 m-5">
    {Array.from({ length: 10 }).map((_, key) => (
      <Skeleton
        key={key}
        className={`h-8 ${key % 2 ? 'w-[100px]' : 'w-[120px]'}`}
      />
    ))}
    {Array.from({ length: 15 }).map((_, key) => (
      <Skeleton
        key={key}
        className={`h-8 ${key % 2 ? 'w-[80px]' : 'w-[90px]'}`}
      />
    ))}
  </section>
);

export default SpeakersProgramme;
