import { Speaker } from '@/components/speaker';
import { Card, CardTitle } from '@/components/ui/card';
import prisma from '@/libs/prisma';
import { PropsWithEventId } from '@/types/page-params';
import { notFound } from 'next/navigation';
import { FC } from 'react';

const SpeakersPage: FC<PropsWithEventId> = async ({ params }) => {
  const { eventId } = await params;

  const speakers = await prisma.speaker.findMany({
    where: {
      schedules: {
        some: {
          eventId
        }
      }
    },
    include: { user: true }
  });

  if (!speakers.length) notFound();

  return (
    <section className="flex flex-col flex-nowrap sm:flex-row sm:flex-wrap justify-center gap-5 m-5">
      {speakers.map((speaker) => (
        <Card key={speaker.userId}>
          <CardTitle className="mx-2">
            <Speaker eventId={eventId} {...speaker} />
          </CardTitle>
        </Card>
      ))}
    </section>
  );
};

export default SpeakersPage;
