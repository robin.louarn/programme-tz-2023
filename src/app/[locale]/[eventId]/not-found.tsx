import { CopyButton } from '@/components/copy-button';
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle
} from '@/components/ui/card';
import { Input } from '@/components/ui/input';
import { environmentsServerParsed } from '@/types/environments-server';
import { getTranslations } from 'next-intl/server';
import { FC } from 'react';

const NotFound: FC = async () => {
  const t = await getTranslations('Index.not_found');

  const url = 'https://tz-[XXX].vercel.app/api/sync';

  return (
    <section className="flex justify-center">
      <Card className="m-5 w-[420px]">
        <CardHeader className="flex items-center">
          <CardTitle>{t('title')}</CardTitle>
        </CardHeader>
        <CardContent className="flex gap-2">
          <Input value={url} readOnly />
          <CopyButton variant="secondary" className="shrink-0" value={url}>
            {t('button')}
          </CopyButton>
        </CardContent>
        <CardFooter>
          <CardDescription>
            {t.rich('description', {
              openplannerEventId: environmentsServerParsed.OPENPLANNER_EVENT_ID,
              b: (chunk) => <b>{chunk}</b>
            })}
          </CardDescription>
        </CardFooter>
      </Card>
    </section>
  );
};

export default NotFound;
