import {
  ScheduleCard,
  ScheduleCardProps,
  scheduleCardVariants
} from '@/components/schedule-card';
import { prisma } from '@/libs/prisma';
import { cn } from '@/libs/utils';
import { environmentsServerParsed } from '@/types/environments-server';
import { getNow, getTranslations } from 'next-intl/server';
import { notFound } from 'next/navigation';
import { FC } from 'react';

const SchedulePage: FC = async () => {
  const t = await getTranslations('Schedule');

  const schedules = await prisma.schedule.findMany({
    where: {
      eventId: environmentsServerParsed.OPENPLANNER_EVENT_ID
    },
    include: {
      category: true,
      format: true,
      speakers: {
        include: { user: true }
      },
      track: true
    },
    orderBy: [
      {
        dateStart: 'asc'
      },
      {
        track: {
          index: 'asc'
        }
      }
    ]
  });

  if (!schedules.length) notFound();

  const now = await getNow();
  const nextDay = await getNow();
  nextDay.setDate(nextDay.getDate() + 1);

  const getScheduleState = (
    start: Date,
    end: Date
  ): ScheduleCardProps['variant'] => {
    switch (true) {
      // comming
      case now < start: {
        return 'default';
      }
      // next day
      case nextDay > end: {
        return 'default';
      }
      // passed
      case now > end: {
        return 'passed';
      }
      // live
      case start >= now && now <= end: {
        return 'default';
      }
    }
  };

  const groupedByDate = schedules.reduce<
    Record<
      string,
      {
        date: string;
        state: ScheduleCardProps['variant'];
        schedules: ScheduleCardProps[];
      }
    >
  >((acc, schedule) => {
    const date = t('schedule', {
      dateStart: schedule.dateStart,
      dateEnd: schedule.dateEnd,
      durationMinutes: schedule.durationMinutes
    });
    const key = schedule.dateStart.toISOString();
    acc[key] = acc[key] || {};
    acc[key].date = date;
    acc[key].state = getScheduleState(schedule.dateStart, schedule.dateEnd);
    acc[key].schedules = acc[key].schedules || [];
    acc[key].schedules.push(schedule);
    return acc;
  }, {});

  return (
    <section className="flex w-full flex-wrap mt-5 gap-5">
      {Object.entries(groupedByDate).map(
        ([key, { date, schedules, state }]) => (
          <div key={key} className="w-full">
            <p className={state === 'passed' ? 'text-muted-foreground' : ''}>
              {date}
            </p>
            <div className={'flex flex-col flex-wrap gap-4 md:flex-row'}>
              {schedules.map((schedule) => (
                <ScheduleCard
                  key={schedule.id}
                  {...schedule}
                  className={cn(
                    scheduleCardVariants({
                      variant: state
                    }),
                    'flex-1'
                  )}
                />
              ))}
            </div>
          </div>
        )
      )}
    </section>
  );
};

export default SchedulePage;
