import { Skeleton } from '@/components/ui/skeleton';
import { FC } from 'react';

const ScheduleProgramme: FC = () => (
  <section className="constainer flex flex-col m-5 gap-5">
    <Skeleton className="h-[20px] w-[60%]" />

    {Array.from({ length: 4 }).map((_, key) => (
      <Skeleton key={key} className="h-[20px] w-[100%]" />
    ))}

    <Skeleton className="h-[20px] w-[40%]" />

    <div className="flex flex-col gap-4">
      {['w-[170px]', 'w-[150px]'].map((w, key) => (
        <Skeleton key={key} className={`h-[20px] ${w}`} />
      ))}
    </div>

    <div className="flex gap-2">
      <Skeleton className="h-[40px] w-[200px]" />
    </div>
  </section>
);

export default ScheduleProgramme;
