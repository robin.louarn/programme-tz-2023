import { Speaker } from '@/components/speaker';
import { Card, CardTitle } from '@/components/ui/card';
import prisma from '@/libs/prisma';
import { environmentsServerParsed } from '@/types/environments-server';
import { PropsWithEventId } from '@/types/page-params';
import { Clock4, MapPin } from 'lucide-react';
import { getTranslations } from 'next-intl/server';
import { FC } from 'react';
import Markdown from 'react-markdown';

type SchedulePageProps = {
  params: Promise<
    {
      scheduleId: string;
    } & Awaited<PropsWithEventId['params']>
  >;
};

const SchedulePage: FC<SchedulePageProps> = async ({ params }) => {
  const { scheduleId, eventId } = await params;
  const t = await getTranslations('Schedule');

  const {
    title,
    abstract,
    dateStart,
    dateEnd,
    durationMinutes,
    speakers,
    track: { name: trackName },
    showInFeedback
  } = await prisma.schedule.findUniqueOrThrow({
    where: {
      id: scheduleId
    },
    include: {
      track: true,
      speakers: { include: { user: true } }
    }
  });

  const openfeedback = new URL(
    `https://openfeedback.io/${environmentsServerParsed.OPEN_FEEDBACK_ID}/2024-12-13/${scheduleId}?hideHeader=true`
  );

  return (
    <section className="flex flex-col p-5 gap-5">
      <h1 className="scroll-m-20 text-2xl font-semibold tracking-tight mb-2">
        {title}
      </h1>
      <Markdown>{abstract}</Markdown>
      <div className="flex gap-2">
        <Clock4 className="inline" />
        {t('schedule', { dateStart, dateEnd, durationMinutes })}
      </div>
      <div className="flex gap-2">
        <MapPin className="inline" /> {trackName}
      </div>
      <div className="flex gap-2">
        {speakers.map((speaker) => (
          <Card key={speaker.userId}>
            <CardTitle className="mx-2">
              <Speaker eventId={eventId} {...speaker} />
            </CardTitle>
          </Card>
        ))}
      </div>
      {showInFeedback && (
        <iframe className="h-96" src={openfeedback.toString()} />
      )}
    </section>
  );
};

export default SchedulePage;
