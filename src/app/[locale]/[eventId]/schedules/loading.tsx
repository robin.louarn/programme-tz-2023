import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle
} from '@/components/ui/card';
import { Skeleton } from '@/components/ui/skeleton';
import { FC } from 'react';

const SchedulesProgramme: FC = () => (
  <section className="flex flex-col flex-wrap mt-5 gap-5">
    <Skeleton className="h-4 w-[100px]" />
    {[1, 3, 2, 3, 1].map((length, rowKey) => (
      <div key={rowKey} className="flex gap-5 flex-wrap">
        {Array.from({ length }).map((_, cardKey) => (
          <Card key={cardKey} className="flex-1">
            <CardHeader>
              <CardTitle className="flex justify-between">
                <Skeleton className="h-5 w-[20%]" />
                <Skeleton className="h-5 w-[20%]" />
              </CardTitle>
            </CardHeader>
            <CardContent className="flex justify-between">
              <Skeleton className="h-5 w-full" />
            </CardContent>
            <CardFooter className="flex justify-center">
              <Skeleton className="h-5 w-[20%]" />
            </CardFooter>
          </Card>
        ))}
      </div>
    ))}
  </section>
);

export default SchedulesProgramme;
