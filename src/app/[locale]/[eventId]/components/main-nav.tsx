'use client';

import { Icons } from '@/components/icons';
import { Link, usePathname } from '@/i18n/routing';
import { cn } from '@/libs/utils';
import { getMainNav, siteConfig } from '@/site';
import { PropsWithEventId } from '@/types/page-params';
import { useTranslations } from 'next-intl';
import { FC } from 'react';

export const MainNav: FC<Awaited<PropsWithEventId['params']>> = ({
  eventId
}) => {
  const pathname = usePathname();
  const t = useTranslations('Index.navigation');

  return (
    <div className="flex gap-6 md:gap-10">
      <Link href="/" className="flex items-center space-x-2">
        <Icons.logo className="h-6 w-6" />
        <span className="inline-block font-bold">{siteConfig.name}</span>
      </Link>
      <nav className="hidden md:flex gap-6">
        {getMainNav(eventId).map(({ key, href, match }) => {
          return (
            <Link
              key={key}
              href={href}
              className={cn(
                'transition-colors hover:text-foreground/80',
                match(pathname, href) ? 'text-foreground' : 'text-foreground/60'
              )}
            >
              {t(key)}
            </Link>
          );
        })}
      </nav>
    </div>
  );
};
