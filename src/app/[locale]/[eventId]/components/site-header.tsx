import { PropsWithEventId } from '@/types/page-params';
import { FC } from 'react';
import { LocaleSwitcher } from './locale-switcher';
import { MainNav } from './main-nav';
import { MobileNav } from './mobile-nav';
import { ThemeToggle } from './theme-toggle';
import { UserButton } from './user-button';

export const SiteHeader: FC<PropsWithEventId> = async ({ params }) => {
  const { eventId } = await params;

  return (
    <header className="sticky top-0 z-40 w-full border-b bg-background">
      <div className="container flex items-center h-16">
        <MobileNav eventId={eventId} />
        <MainNav eventId={eventId} />
        <nav className="flex-1 flex items-center justify-end space-x-1">
          <UserButton />
          <ThemeToggle />
          <LocaleSwitcher />
        </nav>
      </div>
    </header>
  );
};
