'use client';

import { Button, ButtonProps } from '@/components/ui/button';
import { signOut } from 'next-auth/react';
import { FC, PropsWithChildren } from 'react';

type SignOutButtonProps = PropsWithChildren<ButtonProps>;

export const SignOutButton: FC<SignOutButtonProps> = ({
  children,
  ...props
}) => (
  <Button {...props} onClick={() => signOut()}>
    {children}
  </Button>
);
