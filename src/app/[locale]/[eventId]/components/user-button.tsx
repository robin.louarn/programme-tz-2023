import { auth } from '@/auth';
import { Avatar, AvatarFallback, AvatarImage } from '@/components/ui/avatar';
import { Button, buttonVariants } from '@/components/ui/button';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger
} from '@/components/ui/dropdown-menu';
import { Link } from '@/i18n/routing';
import prisma from '@/libs/prisma';
import { initials } from '@/libs/utils';
import { LogOut, User } from 'lucide-react';
import { getTranslations } from 'next-intl/server';
import { FC } from 'react';
import { SignOutButton } from './sign-out-button';

export const UserButton: FC = async () => {
  const session = await auth();
  const t = await getTranslations('Index');

  const user = await prisma.user.findUnique({
    where: {
      id: session?.userId ?? ''
    }
  });

  if (!user)
    return (
      <Link href="/login" rel="noreferrer">
        <div
          className={buttonVariants({
            size: 'icon',
            variant: 'ghost'
          })}
        >
          <User className="h-5 w-5" />
          <span className="sr-only">{t('gitlab_button')}</span>
        </div>
      </Link>
    );

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant="ghost" className="relative w-8 h-8 rounded-full">
          <Avatar className="w-8 h-8">
            {user.image && (
              <AvatarImage src={user.image} alt={user.name ?? ''} />
            )}
            <AvatarFallback>{initials(user.name)}</AvatarFallback>
          </Avatar>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent className="w-56" align="end" forceMount>
        <DropdownMenuLabel className="font-normal">
          <div className="flex flex-col space-y-1">
            <p className="text-sm font-medium leading-none">{user?.name}</p>
            <p className="text-xs leading-none text-muted-foreground">
              {user?.email}
            </p>
          </div>
        </DropdownMenuLabel>
        <DropdownMenuItem>
          <Link href="/settings" className="flex-1">
            {t('user_button.settings')}
          </Link>
        </DropdownMenuItem>
        <DropdownMenuItem>
          <SignOutButton>
            <LogOut /> {t('user_button.sign_out')}
          </SignOutButton>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
