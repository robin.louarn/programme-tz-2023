'use client';

import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue
} from '@/components/ui/select';
import { locales, usePathname, useRouter } from '@/i18n/routing';
import { useLocale, useTranslations } from 'next-intl';
import { FC } from 'react';

export const LocaleSwitcher: FC = () => {
  const t = useTranslations('Index.switch_locale');
  const locale = useLocale();
  const pathname = usePathname();
  const { replace } = useRouter();

  return (
    <div>
      <Select
        defaultValue={locale}
        onValueChange={(nextLocale: (typeof locales)[number]) =>
          replace(pathname, { locale: nextLocale })
        }
      >
        <SelectTrigger>
          <SelectValue placeholder={t('placeholder')} />
        </SelectTrigger>
        <SelectContent>
          {locales.map((value) => (
            <SelectItem key={value} value={value}>
              {value}
            </SelectItem>
          ))}
        </SelectContent>
      </Select>
    </div>
  );
};
