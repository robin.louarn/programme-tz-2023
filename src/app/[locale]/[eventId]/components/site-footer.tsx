import { siteConfig } from '@/site';
import { environmentsServerParsed } from '@/types/environments-server';
import { getTranslations } from 'next-intl/server';
import { FC } from 'react';

export const SiteFooter: FC = async () => {
  const t = await getTranslations('Index');
  return (
    <footer className="py-6 md:px-8 md:py-0">
      <div className="container flex flex-col items-center justify-between gap-4 md:h-24 md:flex-row">
        <p className="text-center text-sm leading-loose text-muted-foreground md:text-left">
          {t.rich('footer', {
            gitlabLink: (chunck) => (
              <a
                href={siteConfig.links.gitlab.project}
                target="_blank"
                rel="noreferrer"
                className="font-medium underline underline-offset-4"
              >
                {chunck}
              </a>
            ),
            userLink: (chunck) => (
              <a
                href={siteConfig.links.gitlab.owner}
                target="_blank"
                rel="noreferrer"
                className="font-medium underline underline-offset-4"
              >
                {chunck}
              </a>
            ),
            openFeedBackLink: (chunck) => (
              <a
                href={`https://openfeedback.io/${environmentsServerParsed.OPEN_FEEDBACK_ID}`}
                target="_blank"
                rel="noreferrer"
                className="font-medium underline underline-offset-4"
              >
                {chunck}
              </a>
            )
          })}
        </p>
      </div>
    </footer>
  );
};
