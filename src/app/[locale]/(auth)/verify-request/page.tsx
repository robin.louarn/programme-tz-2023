import { buttonVariants } from '@/components/ui/button';
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle
} from '@/components/ui/card';
import { Link } from '@/i18n/routing';
import { getTranslations } from 'next-intl/server';
import { FC } from 'react';

const VerifyRequestPage: FC = async () => {
  const t = await getTranslations('VerifyRequest');
  return (
    <section className="flex min-h-[350px] justify-center items-center">
      <Card className="min-w-[350px] m-5">
        <CardHeader>
          <CardTitle>{t('title')}</CardTitle>
        </CardHeader>
        <CardContent>{t('description')}</CardContent>
        <CardFooter>
          <Link href="/" className={buttonVariants()}>
            {t('footer')}
          </Link>
        </CardFooter>
      </Card>
    </section>
  );
};

export default VerifyRequestPage;
