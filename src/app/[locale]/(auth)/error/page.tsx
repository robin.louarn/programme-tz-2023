import { Card, CardContent } from '@/components/ui/card';
import { FC } from 'react';

const ErrorPage: FC = async (props) => {
  return (
    <section className="flex min-h-[350px] justify-center items-center">
      <Card className="min-w-[350px] m-5">
        <CardContent>
          <pre>{JSON.stringify(props, null, 2)}</pre>
        </CardContent>
      </Card>
    </section>
  );
};

export default ErrorPage;
