'use client';

import { Icons } from '@/components/icons';
import { SubmitButton } from '@/components/submit-button';
import { Button } from '@/components/ui/button';
import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { zodResolver } from '@hookform/resolvers/zod';
import { signIn } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { FC } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { z } from 'zod';

const LoginPage: FC = () => {
  const t = useTranslations('Login');

  const searchParams = useSearchParams();

  const loginFormSchema = z.object({
    email: z
      .string({
        required_error: t('form.email.required')
      })
      .email()
  });

  type LoginFormValues = z.infer<typeof loginFormSchema>;

  const form = useForm<LoginFormValues>({
    resolver: zodResolver(loginFormSchema),
    mode: 'onChange',
    defaultValues: {
      email: ''
    }
  });

  const { handleSubmit } = form;

  const onSubmit: SubmitHandler<LoginFormValues> = async (data) => {
    await signIn('resend', {
      callbackUrl: searchParams.get('callbackUrl') ?? '/',
      ...data
    });
  };

  return (
    <section className="flex min-h-[350px] justify-center items-center">
      <Card className="min-w-[350px] m-5">
        <CardHeader className="space-y-1">
          <CardTitle className="text-2xl text-center">{t('title')}</CardTitle>
        </CardHeader>
        <CardContent className="grid gap-6">
          <Form {...form}>
            <form onSubmit={handleSubmit(onSubmit)} className="grid gap-6">
              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('form.email.label')}</FormLabel>
                    <FormControl>
                      <Input placeholder={t('form.email.label')} {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <SubmitButton>{t('form.submit')}</SubmitButton>
            </form>
          </Form>
          <div className="relative">
            <div className="absolute inset-0 flex items-center">
              <span className="w-full border-t" />
            </div>
            <div className="relative flex justify-center text-xs uppercase">
              <span className="bg-background px-2 text-muted-foreground">
                {t('alternative')}
              </span>
            </div>
          </div>
          <Button
            variant="outline"
            className="flex-1"
            onClick={() =>
              signIn('google', {
                callbackUrl: searchParams.get('callbackUrl') ?? '/'
              })
            }
          >
            <Icons.google className="mr-2 h-4 w-4" />
            {t('google_button')}
          </Button>
        </CardContent>
      </Card>
    </section>
  );
};

export default LoginPage;
