import { Separator } from '@/components/ui/separator';
import { getTranslations } from 'next-intl/server';
import { FC, PropsWithChildren } from 'react';
import { SidebarNav } from './components/sidebar-nav';

type SettingsLayoutProps = PropsWithChildren;

const SettingsLayout: FC<SettingsLayoutProps> = async ({ children }) => {
  const t = await getTranslations('Settings');

  return (
    <>
      <div className="space-y-6 p-10 pb-16">
        <div className="space-y-0.5">
          <h2 className="text-2xl font-bold tracking-tight">{t('title')}</h2>
          <p className="text-muted-foreground">{t('sub_title')}</p>
        </div>
        <Separator className="my-6" />
        <div className="flex flex-col space-y-8 lg:flex-row lg:space-x-12 lg:space-y-0">
          <aside className="-mx-4 lg:w-1/5">
            <SidebarNav />
          </aside>
          <div className="flex-1 lg:max-w-2xl">{children}</div>
        </div>
      </div>
    </>
  );
};

export default SettingsLayout;
