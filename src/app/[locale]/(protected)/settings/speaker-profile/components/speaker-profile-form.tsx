'use client';

import { SubmitButton } from '@/components/submit-button';
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage
} from '@/components/ui/form';
import { toast } from '@/components/ui/use-toast';
import { toastActionError } from '@/libs/utils';
import { zodResolver } from '@hookform/resolvers/zod';
import { Speaker } from '@prisma/client';
import MDEditor from '@uiw/react-md-editor';
import { useTranslations } from 'next-intl';
import { useTheme } from 'next-themes';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { updateSpeaker } from './actions';
import {
  SpeakerProfileFormValues,
  getSpeakerProfileFormSchema
} from './shemas';

type SpeakerProfileFormProps = Speaker;

export const SpeakerProfileForm: FC<SpeakerProfileFormProps> = ({ bio }) => {
  const t = useTranslations('SpeakerProfile.form');

  const { theme } = useTheme();

  const profileFormSchema = getSpeakerProfileFormSchema();

  const form = useForm<SpeakerProfileFormValues>({
    resolver: zodResolver(profileFormSchema),
    defaultValues: {
      bio: bio ?? undefined
    },
    mode: 'onChange'
  });

  const onSubmit = async (data: SpeakerProfileFormValues) => {
    const error = await updateSpeaker(data);

    if (error) {
      toastActionError(error);
      return;
    }

    toast({
      title: t('success'),
      description: (
        <pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
          <code className="text-white">{JSON.stringify(data, null, 2)}</code>
        </pre>
      )
    });
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        <FormField
          control={form.control}
          name="bio"
          render={({ field }) => (
            <FormItem>
              <FormLabel>{t('bio.label')}</FormLabel>
              <FormControl data-color-mode={theme}>
                <MDEditor {...field} />
              </FormControl>
              <FormDescription>{t('bio.description')}</FormDescription>
              <FormMessage />
            </FormItem>
          )}
        />

        <SubmitButton>{t('submit')}</SubmitButton>
      </form>
    </Form>
  );
};
