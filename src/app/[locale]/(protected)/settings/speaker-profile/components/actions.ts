'use server';

import { getAbility } from '@/libs/get-ability';
import prisma from '@/libs/prisma';
import { ActionError } from '@/types/actions';
import { ForbiddenError, subject } from '@casl/ability';
import { getLocale } from 'next-intl/server';
import { revalidatePath } from 'next/cache';
import {
  SpeakerProfileFormValues,
  getSpeakerProfileFormSchema
} from './shemas';

export const updateSpeaker = async (
  data: SpeakerProfileFormValues
): Promise<ActionError<SpeakerProfileFormValues> | void> => {
  const dataParsed = getSpeakerProfileFormSchema().safeParse(data);
  const locale = getLocale();

  if (!dataParsed.success) return { validation: dataParsed.error };

  const { ability, session } = await getAbility();

  const speaker = await prisma.speaker.findUniqueOrThrow({
    where: {
      userId: session?.userId
    }
  });

  const forbiddenError = ForbiddenError.from(ability).unlessCan(
    'update',
    subject('Speaker', speaker)
  );

  if (forbiddenError) return { permission: forbiddenError.message };

  await prisma.speaker.update({
    where: {
      userId: session?.userId
    },
    data
  });

  revalidatePath(`${locale}/settings`);
};
