import * as z from 'zod';

export const getSpeakerProfileFormSchema = () =>
  z.object({
    bio: z.string().max(160).min(4)
  });

export type SpeakerProfileFormValues = z.infer<
  ReturnType<typeof getSpeakerProfileFormSchema>
>;
