import { auth } from '@/auth';
import { Separator } from '@/components/ui/separator';
import prisma from '@/libs/prisma';
import { getTranslations } from 'next-intl/server';
import { notFound } from 'next/navigation';
import { FC } from 'react';
import { SpeakerProfileForm } from './components/speaker-profile-form';

const SettingsProfilePage: FC = async () => {
  const t = await getTranslations('SpeakerProfile');
  const session = await auth();

  const speaker = await prisma.speaker.findUnique({
    where: {
      userId: session?.userId
    }
  });

  if (!speaker) notFound();

  return (
    <div className="space-y-6">
      <div>
        <h3 className="text-lg font-medium">{t('title')}</h3>
        <p className="text-sm text-muted-foreground">{t('sub_title')}</p>
      </div>
      <Separator />
      <SpeakerProfileForm {...speaker} />
    </div>
  );
};

export default SettingsProfilePage;
