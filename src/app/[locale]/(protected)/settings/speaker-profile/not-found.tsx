import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card';
import { getTranslations } from 'next-intl/server';
import { FC } from 'react';

const NotFound: FC = async () => {
  const t = await getTranslations('SpeakerProfile.not_found');

  return (
    <section className="flex justify-center">
      <Card className="m-5 w-[420px]">
        <CardHeader className="flex items-center">
          <CardTitle>{t('title')}</CardTitle>
        </CardHeader>
        <CardContent className="flex gap-2">{t('description')}</CardContent>
      </Card>
    </section>
  );
};

export default NotFound;
