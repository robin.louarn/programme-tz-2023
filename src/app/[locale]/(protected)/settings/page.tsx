import { auth } from '@/auth';
import { Separator } from '@/components/ui/separator';
import prisma from '@/libs/prisma';
import { getTranslations } from 'next-intl/server';
import { FC } from 'react';
import { ProfileForm } from './components/profile-form';

const SettingsProfilePage: FC = async () => {
  const t = await getTranslations('UserProfile');
  const session = await auth();

  const user = await prisma.user.findUniqueOrThrow({
    where: {
      id: session?.userId
    }
  });

  return (
    <div className="space-y-6">
      <div>
        <h3 className="text-lg font-medium">{t('title')}</h3>
        <p className="text-sm text-muted-foreground">{t('sub_title')}</p>
      </div>
      <Separator />
      <ProfileForm {...user} />
    </div>
  );
};

export default SettingsProfilePage;
