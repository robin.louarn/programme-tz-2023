'use client';

import { SubmitButton } from '@/components/submit-button';
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { toast } from '@/components/ui/use-toast';
import { toastActionError } from '@/libs/utils';
import { zodResolver } from '@hookform/resolvers/zod';
import { User } from '@prisma/client';
import { useTranslations } from 'next-intl';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { updateUser } from './actions';
import { ProfileFormValues, getProfileFormSchema } from './shemas';

export type ProfileFormProps = User;

export const ProfileForm: FC<ProfileFormProps> = ({ id, email, name }) => {
  const t = useTranslations('UserProfile.form');

  const profileFormSchema = getProfileFormSchema(t);

  const form = useForm<ProfileFormValues>({
    resolver: zodResolver(profileFormSchema),
    defaultValues: {
      email: email ?? undefined,
      name: name ?? undefined
    },
    mode: 'onChange'
  });

  const onSubmit = async (data: ProfileFormValues) => {
    const error = await updateUser(id, data);

    if (error) {
      toastActionError(error);
      return;
    }

    toast({
      title: t('success'),
      description: (
        <pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
          <code className="text-white">{JSON.stringify(data, null, 2)}</code>
        </pre>
      )
    });
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem>
              <FormLabel>{t('name.label')}</FormLabel>
              <FormControl>
                <Input placeholder={t('name.placeholder')} {...field} />
              </FormControl>
              <FormDescription>{t('name.description')}</FormDescription>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="email"
          render={({ field }) => (
            <FormItem>
              <FormLabel>{t('email.label')}</FormLabel>
              <FormControl>
                <Input placeholder={t('email.placeholder')} {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <SubmitButton>{t('submit')}</SubmitButton>
      </form>
    </Form>
  );
};
