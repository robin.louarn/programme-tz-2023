'use server';

import { getAbility } from '@/libs/get-ability';
import prisma from '@/libs/prisma';
import { ActionError } from '@/types/actions';
import { ForbiddenError, subject } from '@casl/ability';
import { getLocale, getTranslations } from 'next-intl/server';
import { revalidatePath } from 'next/cache';
import { ProfileFormValues, getProfileFormSchema } from './shemas';

export const updateUser = async (
  id: string,
  data: ProfileFormValues
): Promise<ActionError<ProfileFormValues> | void> => {
  const t = await getTranslations('UserProfile.form');
  const dataParsed = getProfileFormSchema(t).safeParse(data);
  const locale = getLocale();

  if (!dataParsed.success)
    return {
      validation: dataParsed.error
    };

  const { ability, session } = await getAbility();

  const user = await prisma.user.findUniqueOrThrow({
    where: {
      id: session?.userId
    }
  });

  const forbiddenError = ForbiddenError.from(ability).unlessCan(
    'update',
    subject('User', user)
  );

  if (forbiddenError) return { permission: forbiddenError.message };

  await prisma.user.update({
    where: {
      id
    },
    data
  });

  revalidatePath(`${locale}/settings`);
  revalidatePath(`${locale}/schedules`);
  revalidatePath(`${locale}/speakers/${id}`);
};
