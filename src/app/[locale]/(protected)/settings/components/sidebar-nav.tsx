'use client';

import { buttonVariants } from '@/components/ui/button';
import { Messages } from '@/i18n/request';
import { Link, usePathname } from '@/i18n/routing';
import { cn } from '@/libs/utils';
import { useTranslations } from 'next-intl';
import { FC } from 'react';

type SettingsNavigation = Array<{
  key: keyof Messages['Settings']['navigation'];
  href: string;
  match: (pathname: string, href: string) => boolean;
}>;

const sidebarNavItems: SettingsNavigation = [
  {
    key: 'user_profile',
    href: '/settings',
    match: (pathname: string, href: string) => pathname === href
  },
  {
    key: 'speaker_profile',
    href: '/settings/speaker-profile',
    match: (pathname: string, href: string) => pathname.startsWith(href)
  }
];

type SidebarNavProps = React.HTMLAttributes<HTMLElement>;

export const SidebarNav: FC<SidebarNavProps> = ({ className, ...props }) => {
  const pathname = usePathname();

  const t = useTranslations('Settings.navigation');

  return (
    <nav
      className={cn(
        'flex space-x-2 lg:flex-col lg:space-x-0 lg:space-y-1',
        className
      )}
      {...props}
    >
      {sidebarNavItems.map(({ match, key, href }) => (
        <Link
          key={href}
          href={href}
          className={cn(
            buttonVariants({ variant: 'ghost' }),
            match(pathname, href)
              ? 'bg-muted hover:bg-muted'
              : 'hover:bg-transparent hover:underline',
            'justify-start'
          )}
        >
          {t(key)}
        </Link>
      ))}
    </nav>
  );
};
