import { useTranslations } from 'next-intl';
import * as z from 'zod';

export const getProfileFormSchema = (
  t: ReturnType<typeof useTranslations<'UserProfile.form'>>
) => {
  return z.object({
    name: z
      .string()
      .min(2, {
        message: t('name.min')
      })
      .max(30, {
        message: t('name.max')
      }),
    email: z
      .string({
        required_error: t('email.required_error')
      })
      .email()
  });
};

export type ProfileFormValues = z.infer<
  ReturnType<typeof getProfileFormSchema>
>;
