import { getMessages } from '@/i18n/request';
import { defaultLocale } from '@/i18n/routing';
import { MetadataRoute } from 'next';
import { createTranslator } from 'next-intl';

const manifest = async (): Promise<MetadataRoute.Manifest> => {
  const messages = await getMessages(defaultLocale);
  const t = createTranslator({
    locale: defaultLocale,
    messages,
    namespace: 'Index'
  });

  return {
    name: t('metadata.title'),
    short_name: t('metadata.title'),
    description: t('metadata.description'),
    start_url: '/',
    display: 'standalone',
    background_color: '#ffffff',
    theme_color: '#de022f',
    icons: [
      {
        src: 'icons/zenika-192x192.png',
        sizes: '192x192',
        type: 'image/png'
      },
      {
        src: 'icons/zenika-512x512.png',
        sizes: '512x512',
        type: 'image/png'
      },
      {
        src: 'icons/zenika-512x512.png',
        sizes: '512x512',
        type: 'image/png'
      },
      {
        src: 'icons/zenika-192x192.png',
        sizes: '192x192',
        type: 'image/png'
      }
    ],
    dir: 'ltr',
    lang: 'fr'
  };
};

export default manifest;
