import { FC, PropsWithChildren } from 'react';
import './globals.css';

type RootLayoutProps = PropsWithChildren;

const RootLayout: FC<RootLayoutProps> = ({ children }) => children;

export default RootLayout;
