import { sync } from '@/libs/sync';
import { NextResponse } from 'next/server';

const headers: HeadersInit = {
  'Access-Control-Allow-Origin': 'https://openplanner.fr',
  'Access-Control-Allow-Methods': 'POST',
  'Access-Control-Allow-Headers': 'authorization'
};

export const OPTIONS = () => {
  return NextResponse.json(
    {
      status: 200
    },
    {
      headers
    }
  );
};

export const POST = async () => {
  await sync();

  return NextResponse.json(
    {
      status: 200
    },
    {
      headers
    }
  );
};
