import messages from '@/messages/fr.json';
import { Formats } from 'next-intl';
import { getRequestConfig } from 'next-intl/server';
import { locales, routing } from './routing';

export type PropsWithLocale<T = unknown> = T & {
  params: Promise<{ locale: string }>;
};

export type Messages = typeof messages;

declare global {
  type IntlMessages = Messages
}

export const timeZone = 'Europe/Paris';

export const getMessages = async (locale: string): Promise<Messages> =>
  (await import(`@/messages/${locale}.json`)).default;

export const formats: Partial<Formats> = {
  dateTime: {
    hourAndMinute: {
      hour: 'numeric',
      minute: 'numeric'
    }
  }
};

export default getRequestConfig(async ({ requestLocale }) => {
  let locale = await requestLocale;

  if (
    !locale ||
    !routing.locales.includes(locale as (typeof locales)[number])
  ) {
    locale = routing.defaultLocale;
  }

  return {
    timeZone,
    messages: await getMessages(locale),
    formats
  };
});
