import { createNavigation } from 'next-intl/navigation';
import { defineRouting } from 'next-intl/routing';

export const locales = ['fr', 'en'] as const;

export const defaultLocale = locales[0];

export const routing = defineRouting({
  locales,
  defaultLocale
});

export const { usePathname, useRouter, Link, redirect } =
  createNavigation(routing);
