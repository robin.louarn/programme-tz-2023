import { environmentsServerParsed } from '@/types/environments-server';
import { Resend } from 'resend';

export const resend = new Resend(environmentsServerParsed.RESEND_API_KEY);
