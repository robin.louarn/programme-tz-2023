import { toast } from '@/components/ui/use-toast';
import { ActionError } from '@/types/actions';
import { clsx, type ClassValue } from 'clsx';
import { twMerge } from 'tailwind-merge';

export const cn = (...inputs: ClassValue[]) => {
  return twMerge(clsx(inputs));
};

export const initials = (name?: string | null) =>
  (name ?? '')
    .split(' ')
    .map((name) => name.at(0))
    .join('');

export const toastActionError = <T>(error: ActionError<T>) => {
  if ('validation' in error) {
    toast({
      title: error.validation.errors.map(({ message }) => message).join(' ')
    });
  }

  if ('permission' in error) {
    toast({
      title: error.permission
    });
  }
};

export const fakeLoading = (time: number) =>
  new Promise((resolved) => setTimeout(resolved, time));
