import { defineAbilityFor } from '@/abilities';
import { auth } from '@/auth';
import { getTranslations } from 'next-intl/server';

export const getAbility = async () => {
  const t = await getTranslations('Permissions');
  const session = await auth();
  return {
    t,
    session,
    ability: await defineAbilityFor(t, session?.userId)
  };
};
