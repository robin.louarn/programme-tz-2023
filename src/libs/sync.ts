'use server';

import { prisma } from '@/libs/prisma';
import { environmentsServerParsed } from '@/types/environments-server';
import { openPlannerSchema } from '@/types/open-planner';

export const sync = async () => {
  const url = new URL(environmentsServerParsed.OPENPLANNER_URL);
  url.searchParams.append('t', `${Date.now()}`);

  const reponse = await fetch(url);

  const {
    event: {
      id: eventId,
      formats,
      tracks,
      categories,
      updatedAt: { nanoseconds },
      ...event
    },
    speakers,
    sessions
  } = openPlannerSchema.parse(await reponse.json());

  await Promise.all([
    prisma.event.upsert({
      where: {
        id: eventId
      },
      create: {
        id: eventId,
        updatedAt: new Date(nanoseconds),
        ...event
      },
      update: {
        updatedAt: new Date(nanoseconds),
        ...event,
        schedules: {
          deleteMany: {
            id: {
              notIn: sessions.map(({ id }) => id)
            }
          }
        }
      }
    }),
    ...formats.map(({ id, ...format }) =>
      prisma.format.upsert({
        where: {
          id
        },
        create: {
          id,
          ...format
        },
        update: format
      })
    ),
    ...categories.map(({ id, ...category }) =>
      prisma.category.upsert({
        where: {
          id
        },
        create: {
          id,
          ...category
        },
        update: category
      })
    ),
    ...speakers.map(
      async ({ id, name, socials, photoUrl, email, ...speaker }) =>
        prisma.user.upsert({
          where: {
            email
          },
          create: {
            id,
            email,
            name,
            image: photoUrl,
            speaker: {
              connectOrCreate: {
                where: {
                  userId: id
                },
                create: {
                  ...speaker,
                  socials: {
                    connectOrCreate: socials.map(({ name, ...social }) => ({
                      where: {
                        id: {
                          name,
                          userId: id
                        }
                      },
                      create: { name, ...social }
                    }))
                  }
                }
              }
            }
          },
          update: {
            id,
            speaker: {
              connectOrCreate: {
                where: {
                  userId: id
                },
                create: {
                  ...speaker,
                  socials: {
                    connectOrCreate: socials.map(({ name, ...social }) => ({
                      where: {
                        id: {
                          name,
                          userId: id
                        }
                      },
                      create: { name, ...social }
                    }))
                  }
                }
              }
            }
          }
        })
    )
  ]);

  await Promise.all([
    ...tracks.map(({ id, ...track }, index) =>
      prisma.track.upsert({
        where: {
          id
        },
        create: {
          id,
          event: {
            connect: {
              id: eventId
            }
          },
          index,
          ...track
        },
        update: {
          event: {
            connect: {
              id: eventId
            }
          },
          index,
          ...track
        }
      })
    )
  ]);

  await Promise.all(
    sessions.map(({ id, speakerIds, ...session }) =>
      prisma.schedule.upsert({
        where: {
          id
        },
        create: {
          id,
          eventId,
          ...(speakerIds && {
            speakers: {
              connect: speakerIds.map((id) => ({ userId: id }))
            }
          }),
          ...session
        },
        update: {
          eventId,
          ...(speakerIds && {
            speakers: {
              connect: speakerIds.map((id) => ({ userId: id }))
            }
          }),
          ...session
        }
      })
    )
  );
};
