import { AbilityBuilder, PureAbility } from '@casl/ability';
import {
  PrismaQuery,
  Subjects,
  WhereInput,
  createPrismaAbility
} from '@casl/prisma';
import { Event, Organizer, Schedule, Speaker, User } from '@prisma/client';
import { createTranslator } from 'next-intl';

export type EventSubject = Event & {
  organizers: Organizer[];
};

export type ScheduleSubject = Schedule & {
  event: EventSubject;
  speakers: Speaker[];
};

export type AppAbility = PureAbility<
  [
    'create' | 'update',
    Subjects<{
      User: User;
      Speaker: Speaker;
      Event: EventSubject;
      Schedule: ScheduleSubject;
    }>
  ],
  PrismaQuery
>;

export const defineAbilityFor = async (
  t: ReturnType<typeof createTranslator<'Permissions'>>,
  userId?: string
) => {
  const { can, cannot, build } = new AbilityBuilder<AppAbility>(
    createPrismaAbility
  );

  if (!userId) return build();

  can(['create', 'update'], ['Event', 'Schedule', 'User', 'Speaker']);

  // ------------------ Event -------------------
  const isEventOrganizer: WhereInput<'Event'> = {
    organizers: {
      some: {
        userId
      }
    }
  };

  cannot('update', 'Event', { NOT: isEventOrganizer }).because(
    t('Event.update')
  );

  // ----------------- Schedule -----------------

  const isScheduleOrganizer: WhereInput<'Schedule'> = {
    event: {
      is: isEventOrganizer
    }
  };

  const isScheduleSpeakers: WhereInput<'Schedule'> = {
    speakers: {
      some: {
        userId
      }
    }
  };

  cannot('create', 'Schedule', { NOT: isScheduleOrganizer }).because(
    t('Schedule.create')
  );

  cannot('update', 'Schedule', {
    NOT: {
      OR: [isScheduleSpeakers, isScheduleOrganizer]
    }
  }).because(t('Schedule.update'));

  // ------------------- User -------------------
  cannot('update', 'User', { NOT: { id: userId } }).because(t('User.update'));

  // ------------------ Speaker -----------------
  cannot('update', 'Speaker', { NOT: { userId } }).because(t('Speaker.update'));

  return build();
};
