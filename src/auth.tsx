import { MagicLinkEmail } from '@/emails/magic-link-email';
import prisma from '@/libs/prisma';
import { resend } from '@/libs/resend';
import { environmentsClientParsed } from '@/types/environments-client';
import { environmentsServerParsed } from '@/types/environments-server';
import { PrismaAdapter } from '@auth/prisma-adapter';
import { OAuth2Client } from 'google-auth-library';
import NextAuth, { DefaultSession, NextAuthConfig } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import Google from 'next-auth/providers/google';
import EmailProvider from 'next-auth/providers/resend';
import { createTranslator } from 'next-intl';
import { z } from 'zod';
import { getMessages } from './i18n/request';
import { defaultLocale } from './i18n/routing';

export const profileSchema = z.object({
  email: z.string(),
  name: z.string(),
  picture: z.string()
});

export const userSchema = z.object({
  email: z.string()
});

export const googleOneTapSchema = z.object({
  credential: z.string()
});

declare module 'next-auth' {
  export interface Session extends DefaultSession {
    userId?: string;
  }
}

export const authOptions: NextAuthConfig = {
  adapter: PrismaAdapter(prisma),
  providers: [
    Google({
      clientId: environmentsClientParsed.NEXT_PUBLIC_GOOGLE_CLIENT_ID,
      clientSecret: environmentsServerParsed.GOOGLE_CLIENT_SECRET,
      allowDangerousEmailAccountLinking: true
    }),
    CredentialsProvider({
      id: 'google-one-tap',
      name: 'google-one-tap',
      credentials: {
        credential: { type: 'text' }
      },
      authorize: async (credentials) => {
        const { credential } = googleOneTapSchema.parse(credentials);

        const googleAuthClient = new OAuth2Client(
          environmentsClientParsed.NEXT_PUBLIC_GOOGLE_CLIENT_ID
        );

        const ticket = await googleAuthClient.verifyIdToken({
          idToken: credential,
          audience: environmentsClientParsed.NEXT_PUBLIC_GOOGLE_CLIENT_ID
        });

        const payload = ticket.getPayload();

        if (!payload) {
          throw new Error('Cannot extract payload from signin token');
        }

        const {
          email,
          sub,
          given_name,
          family_name,
          email_verified,
          picture: image
        } = payload;

        if (!email) {
          throw new Error('Email not available');
        }

        const user = await prisma.user.upsert({
          where: {
            email
          },
          create: {
            name: [given_name, family_name].join(' '),
            email,
            image,
            emailVerified: email_verified ? new Date() : null
          },
          update: {}
        });

        await prisma.account.upsert({
          where: {
            provider_providerAccountId: {
              provider: 'google',
              providerAccountId: sub
            }
          },
          create: {
            userId: user.id,
            type: 'credential',
            provider: 'google',
            providerAccountId: sub
          },
          update: {}
        });

        return user;
      }
    }),
    EmailProvider({
      sendVerificationRequest: async (params) => {
        const messages = await getMessages(defaultLocale);
        const t = createTranslator({
          locale: defaultLocale,
          messages,
          namespace: 'MagicLinkEmail'
        });

        await resend.emails.send({
          from: environmentsServerParsed.RESEND_FROM,
          to: params.identifier,
          subject: t('subject'),
          react: <MagicLinkEmail magicLink={params.url} />
        });
      }
    })
  ],
  callbacks: {
    async session({ session }) {
      const safeUser = userSchema.safeParse(session.user);
      if (safeUser.success) {
        const {
          data: { email }
        } = safeUser;
        const { id } = await prisma.user.findUniqueOrThrow({
          where: {
            email
          }
        });
        session.userId = id;
      }

      return session;
    }
  },
  session: {
    strategy: 'jwt'
  },
  pages: {
    signIn: 'login',
    verifyRequest: '/verify-request',
    error: '/error'
  }
};

export const {
  handlers: { GET, POST },
  auth,
  signIn,
  signOut
} = NextAuth(authOptions);
