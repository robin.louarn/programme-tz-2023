import {
  Body,
  Container,
  Head,
  Heading,
  Html,
  Link,
  Preview,
  Section,
  Text
} from '@react-email/components';
import { createTranslator } from 'next-intl';
import { FC } from 'react';
import { defaultLocale } from '../i18n/routing';
import messages from '../messages/fr.json';

type MagicLinkEmailProps = {
  magicLink: string;
};

type EmailFC<T> = FC<T> & { PreviewProps?: T };

export const MagicLinkEmail: EmailFC<MagicLinkEmailProps> = ({ magicLink }) => {
  const t = createTranslator({
    locale: defaultLocale,
    messages,
    namespace: 'MagicLinkEmail'
  });

  return (
    <Html>
      <Head />
      <Preview>{t('preview')}</Preview>
      <Body style={main}>
        <Container style={container}>
          <Heading style={heading}> {t('heading')}</Heading>
          <Section style={body}>
            <Text style={paragraph}>
              <Link style={link} href={magicLink}>
                {t('link')}
              </Link>
            </Text>
            <Text style={paragraph}>{t('ignore')} </Text>
          </Section>
        </Container>
      </Body>
    </Html>
  );
};

MagicLinkEmail.PreviewProps = {
  magicLink: 'https://gitlab.com/robin.louarn/programme-tz-2023'
};

export default MagicLinkEmail;

const main = {
  backgroundColor: '#ffffff',
  fontFamily:
    '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif'
};

const container = {
  margin: '0 auto',
  padding: '20px 25px 48px',
  backgroundImage: 'url("/assets/raycast-bg.png")',
  backgroundPosition: 'bottom',
  backgroundRepeat: 'no-repeat, no-repeat'
};

const heading = {
  fontSize: '28px',
  fontWeight: 'bold',
  marginTop: '48px'
};

const body = {
  margin: '24px 0'
};

const paragraph = {
  fontSize: '16px',
  lineHeight: '26px'
};

const link = {
  color: '#FF6363'
};
