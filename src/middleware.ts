import { defaultLocale, locales } from '@/i18n/routing';
import NextAuth from 'next-auth';
import createIntlMiddleware from 'next-intl/middleware';
import { NextResponse } from 'next/server';

const privatePathnameRegex = RegExp(
  `^(/(${locales.join('|')}))?(${['/settings', '/settings/.*']
    .flatMap((p) => (p === '/' ? ['', '/'] : p))
    .join('|')})/?$`,
  'i'
);

const authPathnameRegex = RegExp(
  `^(/(${locales.join('|')}))?(${['/login', '/verify-request', '/error']
    .flatMap((p) => (p === '/' ? ['', '/'] : p))
    .join('|')})/?$`,
  'i'
);

const { auth: authMiddleware } = NextAuth({
  providers: [],
  callbacks: {
    authorized: async ({
      auth,
      request: {
        nextUrl: { pathname, origin }
      }
    }) => {
      const isPrivatePage = privatePathnameRegex.test(pathname);
      const isAuthPage = authPathnameRegex.test(pathname);

      if (!auth && isPrivatePage)
        return NextResponse.redirect(new URL('/login', origin));

      if (auth && isAuthPage)
        return NextResponse.redirect(new URL('/', origin));
    }
  }
});

const intlMiddleware = createIntlMiddleware({
  locales,
  defaultLocale
});

export default authMiddleware((req) => intlMiddleware(req));

export const config = {
  matcher: ['/((?!api|_next|_vercel|.*\\..*).*)']
};
