import { defineAbilityFor } from '@/abilities';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';

export const useAbility = () => {
  const t = useTranslations('Permissions');
  const { data: session } = useSession();
  return {
    t,
    session,
    ability: defineAbilityFor(t, session?.userId)
  };
};
