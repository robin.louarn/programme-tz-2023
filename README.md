# programme-tz-2023

## C'est quoi ce projet ?

Ce projet permet d'afficher et de partager le programme d'une Technozaure Zenika.

Le CFP est géré par [conference-hall](https://conference-hall.io/), le planning par [openplanner](https://openplanner.fr/), et enfin l'affichage par cette petite application :)

C'est aussi un projet perso à la base, mais qui à l'avantage de resortir chaque année au moment de la TZ et donc de ne pas mourir aussi vite que les autres ^^'

Ça reste donc mon petit labo, un endroit au je test des choses, certaines versions beta... Mon but est d'avoir le projet le plus simple possible, d'avoir la meilleur DX possible et d'utiliser au maximum les solutions avec des plans gratuits.

Les décisions que je prend sont dicté par mes petites envie du moment, mais je reste ouvert à des contributions, ce projet peut aussi devenir votre labo ;)

## Features

- Next.js 14 App Directory (deployé sur vercel)
- Shadcn ui (Radix UI Primitives, Tailwind CSS, Lucide icons)
- Dark mode with `next-themes`
- Prisma
- Nextauth (google provider avec "one tap auth", et Email provider)
- Next-intl
- React-email (utilisation de resend)

## Tooling

- Renovate
- Sementic release
- Husky / Lint-staged

## Usage

> Vous aurrez besoin d'avoir pnpm, docker, docker-compose d'installé.

```shell
cp .env.example .env # prisma ne reconnait pas le .env.local
pnpm install
pnpm db:dev
pnpm docker:dev
pnpm db:migrate
pnpm db:reset
pnpm app:dev
```

> Pour faire fonctionner en local le webhook de openplanner, je vous conseil d'utiliser : ngrok, localtunel...

> Sur MAC M1 décommenter dans le docker-compose la clé platform :

```
  platform: linux/arm64
```

## Contribuer

Pour contribuer il faut ouvir une issue, et créer un merge request.

J'aime beacoup les [gitmoji](https://gitmoji.dev/), vous pourrez les retrouver dans la ci, mais aussi les différentes conventions mises en place sur ce projet.

Pour le titre des issues utiliser cette convention `[gitmoji] [votre titre]` example : `📝 Mettre à jour le README.md`.

Lors de la création de la merge request, supprimer "resolved": `Draft: Resolve "📝 Mettre à jour le README.md"` -> `Draft: 📝 Mettre à jour le README.md`

Pour les commit voici la convention :

```text
[gitmoji] [votre titre]

issues: [id des issues] (optionnel)
```

Exemple :

```text
📝 update README.md

issues: #1212, #1232
```

## Licence

Autorisé sous [MIT Licence](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/LICENSE).
