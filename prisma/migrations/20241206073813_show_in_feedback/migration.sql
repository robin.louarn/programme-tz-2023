-- AlterTable
ALTER TABLE "Schedule" ADD COLUMN     "showInFeedback" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "_ScheduleToSpeaker" ADD CONSTRAINT "_ScheduleToSpeaker_AB_pkey" PRIMARY KEY ("A", "B");

-- DropIndex
DROP INDEX "_ScheduleToSpeaker_AB_unique";
