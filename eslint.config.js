import { FlatCompat } from '@eslint/eslintrc';
import eslint from '@eslint/js';
import eslintConfigPrettier from 'eslint-config-prettier';
import tseslint from 'typescript-eslint';

const compat = new FlatCompat();

export default tseslint.config(
  eslint.configs.recommended,
  tseslint.configs.recommended,
  compat.extends('next'),
  eslintConfigPrettier,
  {
    ignores: ['node_modules/*', 'tailwind.config.cjs', '.next']
  }
);
