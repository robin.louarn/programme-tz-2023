include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - project: 'renovate-bot/renovate-runner'
    file: '/templates/renovate.gitlab-ci.yml'

stages:
  - 🛠️ setup
  - ✅ check
  - 🤖 bot

default:
  interruptible: true

.node:
  image: node:22.0.0
  before_script:
    - corepack enable
    - corepack prepare pnpm@latest-8 --activate
    - pnpm config set store-dir .pnpm-store
  cache:
    key:
      files:
        - pnpm-lock.yaml
        - prisma/schema.prisma
    policy: pull
    paths:
      - .pnpm-store
      - node_modules

🛠️ install:
  stage: 🛠️ setup
  extends: [.node]
  script:
    - pnpm install --frozen-lockfile
  cache:
    policy: pull-push
  rules:
    - changes:
        - pnpm-lock.yaml
        - prisma/schema.prisma

🛠️ env:
  stage: 🛠️ setup
  script:
    - mv $env .env
  artifacts:
    reports:
      dotenv: .env

🤝 e2e:
  stage: ✅ check
  image: mcr.microsoft.com/playwright:v1.43.1-jammy
  extends: [.node]
  services:
    - name: postgres
      alias: db
  variables:
    CI: 'true'
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    POSTGRES_URL_NON_POOLING: postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@db:5432/postgres
  script:
    - pnpm db:migrate
    - pnpm db:seed
    - pnpm app:dev &
    - pnpm e2e:test
  artifacts:
    when: always
    expose_as: playwright
    paths:
      - test-results
      - playwright-report/index.html
    reports:
      junit: playwright-report/results.xml

✅ test:
  stage: ✅ check
  extends: [.node]
  script:
    - pnpm test

renovate:
  stage: 🤖 bot
  variables:
    RENOVATE_GIT_AUTHOR: Renovate Bot <bot@renovateapp.com>
    RENOVATE_EXTRA_FLAGS: $CI_PROJECT_PATH
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

🔖 release:
  stage: 🤖 bot
  extends: [.node]
  script:
    - pnpm exec semantic-release
  only:
    - main
  when: manual
